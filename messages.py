from flask import Flask, request
from flask_restful import Resource, Api
import hashlib

#Definge Flask App


app = Flask(__name__)
api = Api(app)


hashDict = {}

class Message(Resource):

    def get(self, hex_dig=None):
        ''' Retrieve msg by it's previously hashed code '''

        print hex_dig
        msg = hashDict.get(hex_dig)
        if msg:
            return {'message': msg}
        else:
            return {'err_msg': 'Message not found'}, 404





class MessageCollection(Resource):

    def post(self):
        ''' Create msg and store by it's hash code '''

        msg = str(request.json['message'])
        hash_object = hashlib.sha256(msg)
        hex_dig = hash_object.hexdigest()
        hashDict[hex_dig] = msg
        print 'in post'
        print request.json
        print 'msg = ',  msg, request.json['message']
        return {'digest': hex_dig}, 200

#define and register url's

api.add_resource(MessageCollection, '/messages')
api.add_resource(Message, '/messages/<string:hex_dig>')

#Start Flask

if __name__ == '__main__':
    app.run(debug=True,port =5051)
