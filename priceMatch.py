import csv
import sys

if len(sys.argv) != 3:
    print "please enter a file and dollar balance"
    sys.exit()

fname = sys.argv[1]
#print fname


# All math in cents
balance = int(sys.argv[2])


#print balance
fieldnames = ['Item', 'Cost'  ]

productList = []
totalList = []

with open(fname, 'rb', ) as f:

    reader = csv.DictReader(f,  fieldnames=fieldnames)

    for row in reader:

         item = row['Item']
         cost = int(row['Cost'])

         #print item
         #print cost

         productItem = (item, cost)
         productList.append(productItem)

    #print productList

    target = balance

    #print 'target = ', target

    count = 0

    for p in range(len(productList)):
        #print p, productList[p]
        for q in range(p+1, len(productList)):
            count +=1
            total = productList[p][1] + productList[q][1]
            #print count , p,  productList[p], q, productList[q], total
            totalItem = (total, p, q)
            totalList.append(totalItem)

    totalList = sorted(totalList)

    #print totalList

    found = False
    left = 0
    right = len(totalList) - 1
    last = 0

    while left <= right and found == False:

        midpoint = (left + right) // 2
        #print totalList[midpoint][0]

        if target == totalList[midpoint][0]:
            found = True
            print totalList[midpoint][0], productList[totalList[midpoint][1]],  productList[totalList[midpoint][2]]
        elif target <  totalList[midpoint][0]:
            right = midpoint - 1
        else:
            last = midpoint
            left = midpoint + 1

    if found == False and  target > totalList[last][0]:
        #print   totalList[midpoint][0]
        #print last
        #print   totalList[last][0]
        print totalList[last][0], productList[totalList[last][1]],  productList[totalList[last][2]]
    elif found == False:
        print "Not possible"
